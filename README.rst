
.. image:: https://badge.fury.io/py/kaiju-redis.svg
    :target: https://pypi.org/project/kaiju-redis
    :alt: Latest package version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

`Python <https://www.python.org>`_ >=3.11

`Redis <https://redis.io>`_ >=7.0

Redis client services, redis cache and RPC streams client and server.
See `documentation <https://kaiju-redis.readthedocs.io/>`_ for more info.

Installation
------------

.. code-block:: console

  pip install kaiju-redis

You need a `kaiju application <https://gitlab.com/kaiju-python/kaiju-base-app>`_ to run these services.
