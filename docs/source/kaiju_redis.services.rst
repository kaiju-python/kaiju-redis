**services** - application services
===================================

.. autoclass:: kaiju_redis.services.RedisTransportService
   :members:
   :exclude-members: init, close, closed, __getattr__
   :show-inheritance:

.. autoclass:: kaiju_redis.services.RedisCacheService
   :members:
   :exclude-members: init, close, closed, discover_service, get_transport_cls
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_redis.services.RedisLocksService
   :members:
   :exclude-members: init, close, closed, discover_service, ErrorCode, get_transport_cls, m_exists
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_redis.services.RedisListener
   :members:
   :exclude-members: init, close, closed, discover_service, service_name
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_redis.services.RedisStreamRPCClient
   :members:
   :exclude-members: init, close, closed, discover_service, Topic, service_name
   :show-inheritance:
   :inherited-members:
