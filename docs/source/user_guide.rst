User guide
==========

Transport
---------

:py:class:`~kaiju_redis.services.RedisTransportService`
supports both `redis <https://redis.io>`_ and `keydb <https://docs.keydb.dev>`_ in a single node
or in a cluster configuration.

Configuration example:

.. code-block:: yaml

  services:
    - cls: RedisTransportService
      settings:
        host: localhost
        port: 6379
        password: redis1
        db: 0
        cluster: False

Cache
-----

:py:class:`~kaiju_redis.services.RedisCacheService` provides a standard cache interface
for a redis transport.

Cache method are
:py:meth:`~kaiju_redis.services.RedisCacheService.exists`, :py:meth:`~kaiju_redis.services.RedisCacheService.m_exists`
:py:meth:`~kaiju_redis.services.RedisCacheService.get`, :py:meth:`~kaiju_redis.services.RedisCacheService.m_get`,
:py:meth:`~kaiju_redis.services.RedisCacheService.set`, :py:meth:`~kaiju_redis.services.RedisCacheService.m_set`,
:py:meth:`~kaiju_redis.services.RedisCacheService.delete`, :py:meth:`~kaiju_redis.services.RedisCacheService.m_delete`.
All methods require namespace keys for ids.

Configuration example:

.. code-block:: yaml

  services:
    - cls: RedisTransportService  # transport is required
    - cls: RedisCacheService

Locks
-----

:py:class:`~kaiju_redis.services.RedisLocksService` provides a standard shared locks interface
for a redis transport.

Locks method are
:py:meth:`~kaiju_redis.services.RedisLocksService.acquire`, :py:meth:`~kaiju_redis.services.RedisLocksService.release`.
All methods require namespace keys for ids.

Configuration example:

.. code-block:: yaml

  services:
    - cls: RedisTransportService  # transport is required
    - cls: RedisLocksService

Streams
-------

Server
______

:py:class:`~kaiju_redis.services.RedisListener` provides
a streaming interface for RPC server. The services use `redis streams <https://redis.io/docs/data-types/streams-tutorial/>`_
and require settings an consumer group and trim / claim policies.

You can use `max_batch_size` setting to customize the batch size or `max_wait_time_ms` to change wait
time between the batches.

.. note::

  Set `max_batch_size` to 1 to disable parallel processing of stream messages.

`pending_messages_time_ms` can be used to automatically claim pending and not acked messages
(see `XAUTOCLAIM <https://redis.io/commands/xautoclaim/>`_).

.. note::

  Keep in mind that a message is considered 'pending' while it is being processed by a server. Do not set this value
  too small to keep the server from processing one message twice.

`trim_size` settings can be customized to change the maximum length of the stream
(see `XTRIM <https://redis.io/commands/xtrim/>`_).

Note that trim by stream size will remove *all* messages exceeding the trim size threshold, including the ones which
haven't been processed yet.

Alternatively you can use `trim_delivered` to trim only delivered messages. This option will disable trim by size
behavior. However you shouldn't use `trim_delivered` if you plan to use your stream as quasi-persistent data stream,
because you will not be able to re-read data from it.

Server configuration example:

.. code-block:: yaml

  services:
    - cls: RedisTransportService  # transport, rpc service and locks are required
    - cls: JSONRPCServer
    - cls: RedisLocksService
    - cls: RedisListener
      name: redis_stream.rpc
      enabled: "[services_stream_rpc_enabled:True]"
      settings:
        group_id: "[main_name]"  # consumer group
        topic: rpc  # topic to listen
        scope: SYSTEM  # override scope to disable RPC authentication in internal streams
        trim_delivered: True
        pending_messages_time_ms: null  # set it to an integer (ms) to reclaim pending and not yet processed messages

Client
______

You can configure and use :py:class:`~kaiju_redis.services.RedisStreamRPCClient` to send RPC requests over a Redis
stream. The client interface is similar to the HTTP RPC client, however it doesn't return results since it's
one-way communication, i.e. all outgoing messages will be automatically marked as "notify" requests.

Usage example:

.. code-block:: python

  from kaiju_redis import RedisStreamRPCClient

  class MyService(ContextableService):

    async def init(self):
      self._stream = self.discover_service(None, cls=RedisStreamRPCClient)

    async def send_rpc_request(self):
      """Send RPC request to other app method (other app must have a Redis Listener and the method must be exposed)."""
      await self._stream.call(
        method='ext_service.do_something',
        params={'value': 123}, app='other_app_name')

    async def send_raw_message(self):
      """Send a raw data to a stream, the data will be msgpack-encoded."""
      topic = self.app.namespace.get_key('some_stream') #: custom topic key
      await self._stream.write(topic, body=42, headers={'Custom-Header': '1'})


The service requires a configured :py:class:`~kaiju_redis.services.RedisTransportService`.

Basic configuration example:

.. code-block:: yaml

  services:
    - cls: RedisTransportService
    - cls: RedisStreamRPCClient
